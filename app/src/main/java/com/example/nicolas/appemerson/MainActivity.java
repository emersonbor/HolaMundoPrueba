package com.example.nicolas.appemerson;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nicolas.appemerson.vista.FormularioActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, editTextPassword;
    private Button buttonLogin;
    private TextView textViewPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.editTextPassword = (EditText) findViewById(R.id.etPassword);

        this.buttonLogin = (Button) findViewById(R.id.btingresar);

        this.textViewPassword = (TextView) findViewById(R.id.tvPassword);


        //trabajar con el boton.

        this.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // obtener contraseña.
                String password = editTextPassword.getText().toString();

                //mostrar contraseña
                textViewPassword.setText(password);

                //Mostrar Toast (mensaje temporal)
                Toast.makeText(getApplicationContext(), "Passwrod: " + password, Toast.LENGTH_SHORT);


            }
        });

        this.textViewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //iniciar la segunda ventana
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                //pedir que se ejecute
                startActivity(nuevaVentana);
            }
        });
    }
}
