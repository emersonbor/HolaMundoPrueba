package com.example.nicolas.appemerson.vista;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.nicolas.appemerson.R;

/**
 * Created by Nicolas on 25-08-2017.
 */

public class FormularioActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_formulario);
    }
}
